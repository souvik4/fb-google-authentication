import {signInWithPopup, FacebookAuthProvider, GoogleAuthProvider} from 'firebase/auth'
import { authentication } from './config/firebase-config'
import './App.css';

function App(){

  const signInWithFacebook = () => {
    const fprovider = new FacebookAuthProvider();
    signInWithPopup(authentication, fprovider).then((res) => {
      const user = res.user
      console.log(user)
    
    }).catch((err) => {
      console.log(err.message);
    })   
  }


    const signInWithGoogle = () => {
      const provider = new GoogleAuthProvider();
      signInWithPopup(authentication, provider).then((res) => {
        console.log(res)
      }).catch((err) => {
        console.log(err.message);
      })
    }


  return(
    <div className='App'>
      <button onClick={signInWithFacebook}>Facebook</button>
      <button onClick={signInWithGoogle}>Google</button>
    </div>
  )

}
export default App;