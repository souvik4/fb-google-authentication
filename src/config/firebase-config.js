import firebase from 'firebase/compat/app'
import 'firebase/compat/firestore'
import 'firebase/compat/database'
import 'firebase/compat/auth';
import 'firebase/compat/storage'
import {initializeApp} from 'firebase/app'
import { getAuth } from 'firebase/auth';


const firebaseConfig = {
    apiKey: "AIzaSyACAppepWhdu4bsHy3ZjGFUO5cFAD5g6PM",
    authDomain: "facebook-connection-7ab15.firebaseapp.com",
    projectId: "facebook-connection-7ab15",
    storageBucket: "facebook-connection-7ab15.appspot.com",
    messagingSenderId: "519097315515",
    appId: "1:519097315515:web:650e08042851ff5a61399e",
    measurementId: "G-WXLYCDM15H"
  };
  

  const app = initializeApp(firebaseConfig)
  export const authentication = getAuth(app)
  export default firebase; 